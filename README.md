# Minimal experimental project

## Features to explore

- Library that builds into both static and sharedlib

- Unit test that can use Library

- Cross platform use of shared library with linker help (import lib etc)

- Code coverage

- Latest Gtest

- C++ client + lib

- C Client + lib

- Better robot test structure

- Alternate build systems

	- CMake

	- Autotools

	- Parts

## Key Learnings

### Include file handling

- Use `.` as CPPPATH for components

- Inside component refer to include files via their part root paths,
  i.e. `include/minimal.h`.

- Outside component refer to include file via its canonical/install
  path.  Unit tests should do this if they are meant to test an
  external library interface.  Note the order of `env.Clone()` and
  CPPPATH append matters!

### Scons/Parts default targets

- `Default('partname')` is not the same as adding partname to
  commandline! Use `Default('name::partname')` instead!
