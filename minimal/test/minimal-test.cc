/*############################################################################
  # Copyright 2017 Intel Corporation
  #
  # Licensed under the Apache License, Version 2.0 (the "License");
  # you may not use this file except in compliance with the License.
  # You may obtain a copy of the License at
  #
  #     http://www.apache.org/licenses/LICENSE-2.0
  #
  # Unless required by applicable law or agreed to in writing, software
  # distributed under the License is distributed on an "AS IS" BASIS,
  # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  # See the License for the specific language governing permissions and
  # limitations under the License.
  ############################################################################*/

///
/*! \file */

extern "C" {
#include "minimal/minimal.h"
}

#include <cstdlib>
#include <iostream>

int main(int argc, char* argv[]) {
  (void)argc;
  (void)argv;
  const int a = 1;
  const int b = 2;
  const int c = 3;
  const int d = 4;
  const int expected_result = 0;
  int actual_result = minimal(a, b, c, d);
  std::cout << "Expected: " << expected_result << "\n"
            << "Actual: " << actual_result << "\n";
  if (actual_result != expected_result) {
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

